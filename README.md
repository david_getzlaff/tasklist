# README #

### Wofür ist dieses Repository? ###

- TaskListe / ToDo-Liste für den Browser zum Manages eines Projektes + (Unter)Aufgaben.
- Verarbeitung erfolgt als JSON.

Reine JavaScript / HTML5-Anwendung! (Kein Server wird benötigt)

### Wie gehts los? ###

- Projekt downloaden
- index.html im Browser öffnen
- Arbeiten

### Funktionen ###
- Export / Import des Projekt-JSONs. Default: Halten der Daten im localSorage.
- Setzen von Prio + Status eines (Sub) Tasks.
- Neuordnen von Tasks / SubTasks (mittels Drag & Drop).
- Auf / Zuklappen von Listen.
- CSS zum Highliten von hochpriorisierten Tasks
- Kommentar pro Task
- Datum pro Task - Hinweis vor Erreichen des Ablaufdatums
