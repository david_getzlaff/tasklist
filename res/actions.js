

function updateAction(){
    loadProject();
}
function saveAction(){
    saveTaskList(listToJSON());
}
function exportAction(){
    let obj = listToJSON();
    let a = create('a');
    a.href = 'data:application/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(obj));
    a.download = `${obj.projekt}-${today()}-${now()}.json`;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}
function importAction(){
    let input = create('input');
    input.setAttribute("type", "file");
    input.setAttribute("id", "files");
    input.setAttribute("name", "file[]");

    function handleFileSelect(evt) {
        let files = evt.target.files; // FileList object
        

        // files is a FileList of File objects. List some properties.
        let output = [];
        for (let i = 0, f; f = files[i]; i++) {
            log("Lade Datei " + escape(f.name));
            //output.push('<li><strong>', escape(f.name), '</strong> (', f.type || 'n/a', ') - ',
            //f.size, ' bytes, last modified: ',
            //f.lastModifiedDate ? f.lastModifiedDate.toLocaleDateString() : 'n/a',
            //'</li>');
                let reader = new FileReader();
                let newProjekt = {};
                // Closure to capture the file information.
                reader.onload = (function(theFile) {
                    return function(e) {
                        try {
                            let base64result = e.target.result;
                            newProjekt = window.atob(base64result.substring(base64result.indexOf("base64,") + "base64,".length));
                            loadProject(JSON.parse(newProjekt));
                        } catch(err) {
                            console.log(err);
                            error("Datei konnte nicht geladen werden -> " + err.toString());
                        }
                    };
                })(f);
                // Read in the image file as a data URL.
                reader.readAsDataURL(f);
                
            
        }
        //document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
    }

    document.body.appendChild(input);
    document.getElementById('files').addEventListener('change', handleFileSelect, false);
    input.click();
    document.body.removeChild(input);
}
