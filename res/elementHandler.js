/**
 * Handler, der auf Events (z. B. Doppelklick) lauscht und Elemente ändert.
 * z. B. Task.Name wird zum Inputfield. Bei speichern wird das JSON aktualisiert.
 */


/**
 * Baut ein DropDown-Menu aus einer elementen.
 * Zugiff auf die einzelnen Elemente: task.<prio|status>.["idx"]
 */
function toDropDown(){
    let listName = this.className.replace("-", ".");
    let itemList = eval(listName);
    //log(`Erstelle Selection für '${this.parentNode.getAttribute("data-task-name")}' -> ${listName}`);

    let listener = create("select");
    listener.name = `update-${this.parentNode.getAttribute("data-task-name")}-${listName}`;
    Object.keys(itemList).forEach((key) => {
        let newOption = create("option");
        newOption.value = key;
        newOption.text = itemList[key];
        listener.appendChild(newOption);
    });
    listener.value = this.getAttribute("property");
    listener.addEventListener('blur', destroyDropDown);
  
    this.innerText = "";
    this.appendChild(listener);
}

function destroyDropDown(){
    let parent = this.parentNode;
    parent.setAttribute("property", this.value);
    parent.innerHTML = getTextFromValue(parent);
    parent.parentNode.setAttribute("data-" + parent.className.replace(".", "-"), this.value);
    this.remove();
}



function toInput(){
    let listener = create("input");
    listener.setAttribute("name", `update-${this.className}`);
    listener.setAttribute("class", "update");
    listener.value = this.innerText;
    listener.addEventListener('blur', destroyInput);
    listener.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            this.blur();
        }
    });
    
    this.setAttribute("evtListener", "toInput");
    this.removeEventListener('dblclick', toInput);
    this.innerText = "";
    this.appendChild(listener);
}

function toTextArea(){
    let listName = this.className.replace("-", ".");
    let parent = this.parentNode;
    //log(`Erstelle TextArea für '${this.parentNode.getAttribute("data-task-name")}' -> ${listName}`);
    let itemList = eval(listName);

    let listener = create("textArea");
    listener.name = `update-${this.parentNode.getAttribute("data-task-name")}-${listName}`;

    listener.addEventListener('blur', destroyInput);
    listener.value = this.parentNode.getAttribute("data-task-name").replace(new RegExp('<br>', 'g'), "\n");
    
    this.setAttribute("evtListener", "toTextArea");
    this.removeEventListener('dblclick', toTextArea);

    
    this.innerText = "";
    this.appendChild(listener);
}

function destroyInput(){
    let parent = this.parentNode;
    //log("destroyInput() -> " + parent.className);
    let newVal = this.value.replace(new RegExp('\n', 'g'), "<br>");
    parent.innerHTML = newVal;
    parent.parentNode.setAttribute("data-" + parent.className.replace(".", "-"), newVal);
    parent.addEventListener("dblclick", eval(parent.getAttribute("evtListener")));
    this.remove();
}



