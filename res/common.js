
function create(text_) {
    return document.createElement(text_);
}


function d2(_n) {
    if( _n < 10 )
        return "0" + _n;
    return _n;
}
function today(c) {
  let _d = new Date();
  c = c || true;
  return ""
        + _d.getFullYear() + (c ? "-" : "")
        + d2(_d.getMonth()+1) + (c ? "-" : "")
        + d2(_d.getDate());
}
function now(c) {
  let _d = new Date();
  c = c || true;
  return ""
        + d2(_d.getHours()) + ( c ? ":" : "")
        + d2(_d.getMinutes()) + ( c ? ":" : "")
        + d2(_d.getSeconds());
}

function log(text_, level_) {
    text_ = text_ || "";
    level_ = level_ || "DEBUG";
    let date_ = today() + " " + now();
    log2div(date_, level_, text_);
    console.log(`${date_} ${level_} :: ${text_}`);
}
function warn(text_) {
    log(text_, "WARN ");
}
function error(text_) {
    log(text_, "ERROR");
}
function ok(text_) {
    log(text_, "SUCCESS");
}


function fadeOut(element, justWait_, doRemove_) {
    justWait_ = justWait_ || 3;
    doRemove_ = doRemove_ || false;
    setTimeout(function(){
        let op = 1; // initial opacity
        let timer = setInterval(function() {
            if (op <= 0.1) {
                clearInterval(timer);
                element.style.display = 'none';
                if (doRemove_) {
                    element.remove();
                }
            }
            element.style.opacity = op;
            element.style.filter = 'alpha(opacity=' + op * 100 + ")";
            op -= op * 0.1;
        }, 25);
    }, justWait_ * 1000);
}
function fadeOutAndRemove(element, justWait_) {
    fadeOut(element, justWait_, true);
}


function log2div(date_, level_, text_) {
    if( typeof(notificationField) !== "undefined" ) {
        let notification = create("div");
        let span_level = create("span");
        span_level.innerHTML = level_ + " ";
        notification.appendChild(span_level);
        
        notification.setAttribute("class", "notification " + level_.toLowerCase());
        notification.innerHTML += date_ + " | " +text_;
        
        notificationField.appendChild(notification);
        if (level_.toUpperCase() !== "ERROR")
            fadeOutAndRemove(notification);
        else 
            fadeOutAndRemove(notification, 15);
    }
};
