

let taskListObjectName = "taskList";  // Name des Objektes im Storage
let listType = "UL"; // [UL|OL]


function getTaskList(){
    return JSON.parse(localStorage.getItem(taskListObjectName));
}

function saveTaskList(taskList2Save) {
    if (typeof(taskList2Save) === "undefined" || taskList2Save == null) {
        error("Kein taskList-Objekt im RAM gefunden!");
    } else {
        localStorage.setItem(taskListObjectName, JSON.stringify(taskList2Save))
        ok("taskList im localStorage gespeichert!");
    }
}

function initTaskList() {
    log("Initialisiere TaskListe..");
    let newtaskList = JSON.parse(`{
	"projekt": "Introvertiert",
	"datum": "2017-10-18 20:36:58",
	"tasks": [{
            "name": "Projektnamen ausdenken",
            "prio": 1,
            "status": 4,
            "tasks": []
        }, {
			"name": "JS lernen",
			"prio": 3,
			"status": 4,
			"tasks": []
		}, {    
			"name": "todo List erstellen",
			"prio": 1,
			"status": 2,
			"tasks": [{
					"name": "Projekt anlegen",
					"prio": 2,
					"status": 2,
					"tasks": [{
                        "name": "Repo anlegen",
                        "prio": 2,
                        "status": 4,
                        "tasks": [{
                            "name": "Repo berechtigen",
                            "prio": 4,
                            "status": 4,
                            "tasks": []                          
                        }]
                    }, {
                        "name": "Jonas ins Boot holen",
                        "prio": 3,
                        "status": 5,
                        "tasks": []
                    }, {
                        "name": "README schreiben",
                        "prio": 2,
                        "status": 4,
                        "tasks": []
                    }]
				}, {
					"name": "Logik überlegen",
					"prio": 2,
					"status": 1,
					"tasks": []
				}
			]
		}
	]
}`);
    saveTaskList(newtaskList);
}

function loadProject(newProject_){
    
    let rootElement = document.getElementById("tasklist");
    
    // Alte Elemente entfernen
    while (rootElement.firstChild) {
        rootElement.removeChild(rootElement.firstChild);
    }
    
    /*  TaskListe aus Storage laden  */
    let taskList = newProject_ || getTaskList();
    
    /*  Projekt-Header erstellen
     *
     * Header ist ein DIV
     * und besteht aus 3 DIVs: Projektname, Datum + Timestamp, Buttons
     * Innerhalb ist der Projektname ein H1
     * Buttons zum Speichern, Laden, Neuladen und Exportieren des Projektes
     */
    let headerWrapper = create("div");
    headerWrapper.setAttribute("class", "projekt");
    
    let HEADER_PROJEKTNAME = create("div")
        , HEADER_PROJEKTDATUM = create("div");
    
    //let projekt_header = create("h1");
    //projekt_header.innerHTML = taskList.projekt;
    //projekt_header.style = "display:inline";
    
    HEADER_PROJEKTNAME.setAttribute("class", "projekt-name");
    HEADER_PROJEKTNAME.addEventListener('dblclick', toInput);
    HEADER_PROJEKTDATUM.setAttribute("class", "projekt-datum");
    HEADER_PROJEKTNAME.innerHTML = taskList.projekt;
    HEADER_PROJEKTDATUM.innerHTML = taskList.datum;
    
    headerWrapper.setAttribute("data-projekt-name", taskList.projekt);
    headerWrapper.setAttribute("data-projekt-datum", taskList.datum);
    headerWrapper.appendChild(HEADER_PROJEKTNAME);
    headerWrapper.appendChild(HEADER_PROJEKTDATUM);
    rootElement.appendChild(headerWrapper);
    
    /* Darstellung des Aufbaus */
    let AUFBAU = create("div");
    AUFBAU.setAttribute("class", "taskaufbau");
    
    
    let DIV_AUFGABE = create("div");
    let DIV_PRIO = create("div");
    let DIV_STATUS = create("div");
    
    let SPAN_AUFGABE = create("span");
    let SPAN_PRIO = create("span");
    let SPAN_STATUS = create("span");
    
    SPAN_AUFGABE.innerHTML = "Aufgabe";
    SPAN_PRIO.innerHTML = "Priorität";
    SPAN_STATUS.innerHTML = "Status";
    
    let btn_neu = create("button");
    btn_neu.addEventListener('click', () => {resolveTask( {"name": "neue Aufgabe", "prio": 3,"status" : 1 } )});
    let btn_neu_img = create("img");
    btn_neu_img.src = src="res/img/add.png";
    
    btn_neu.appendChild(btn_neu_img);
    DIV_AUFGABE.appendChild(btn_neu);
    
    
    DIV_AUFGABE.appendChild(SPAN_AUFGABE);
    DIV_PRIO.appendChild(SPAN_PRIO);
    DIV_STATUS.appendChild(SPAN_STATUS);
    
    AUFBAU.appendChild(DIV_AUFGABE);
    AUFBAU.appendChild(DIV_PRIO);
    AUFBAU.appendChild(DIV_STATUS);
    
    rootElement.appendChild(create("hr"));
    rootElement.appendChild(AUFBAU);
    
    /* Auflistung der Tasks */
    let UL = create(listType);
    UL.setAttribute("class", "projekt");
    UL.setAttribute("name", "rootList");
    rootElement.appendChild(UL);
    
    /*  Tasks anhängen  */
    let childCount = 2;
    let tasks = taskList.tasks;
    if (Array.isArray(tasks) && tasks.length > 0) {
        Object.keys(tasks).forEach(function(key) {
            let task = tasks[key];
        //    log(`@ ${UL.getAttribute("data-name")} |  add Child ${childCount} / ${tasks.length} : ${task-name}`);
            resolveTask(task);
            childCount++;
        });
    }
    
    document.title = "Tasks @ " + taskList.projekt;
    log(`Projekt ${taskList.projekt} wurde geladen!`);
}

function resolveTask(task_, parent_){
    //log(`  - ${parent_.getAttribute("data-name")} + Child ${childCount} / ${tasks.length} : ${task_.name}`);
    //log(` + ${task_.name} @ ${parent_.getAttribute("data-name")}`);
    
    parent_ = parent_ || document.getElementsByName("rootList")[0];

    let newTask = create("li");
    
    buildTaskOptions(newTask);
    
    let DIV = create("div");
    DIV.setAttribute("class", "task");
    DIV.setAttribute("data-task-name", task_.name);
    DIV.setAttribute("data-task-prio", task_.prio);
    DIV.setAttribute("data-task-status", task_.status);
    DIV.style="display: inline-grid; width:100%";
   
    let SPAN_NAME = create("span");
    let SPAN_PRIO = create("span");
    let SPAN_STATUS = create("span");
    
    SPAN_NAME.innerHTML = task_.name;
    SPAN_NAME.className = "task-name";
    
    SPAN_PRIO.innerHTML = task_.prio;
    SPAN_PRIO.className = "task-prio";
    SPAN_PRIO.setAttribute("property", task_.prio);
    SPAN_PRIO.innerHTML = getTextFromValue(SPAN_PRIO);
    
    SPAN_STATUS.innerHTML = task_.status;
    SPAN_STATUS.className = "task-status";
    SPAN_STATUS.setAttribute("property", task_.status);
    SPAN_STATUS.innerHTML = getTextFromValue(SPAN_STATUS);
    
    /*  Listener auf die Elemente definiern, damit sie geändert werden können. */
    SPAN_NAME.addEventListener('dblclick', toTextArea);
    SPAN_PRIO.addEventListener('dblclick', toDropDown);
    SPAN_STATUS.addEventListener('dblclick', toDropDown);
   
    DIV.appendChild(SPAN_NAME);
    DIV.appendChild(SPAN_PRIO);
    DIV.appendChild(SPAN_STATUS);
    
    newTask.appendChild(DIV);
    parent_.appendChild(newTask);
    
    
    
    /*  Tasks anhängen  */
    let childCount = 1;
    let tasks = task_.tasks;
    if (Array.isArray(tasks) && tasks.length > 0) {
        // neues LI anlegen, in dem eine neue UL für die Subtasks liegt.
        let subList = create(listType);
        // subList.setAttribute("data-task-name", "-" + parent_firstChild.getAttribute("data-task-name"));

        Object.keys(tasks).forEach(function(key) {
            let task = tasks[key];
        //    log(`  ${subList.getAttribute("data-name")} |  add Child ${childCount} / ${tasks.length} : ${task_.name}`);
            resolveTask(task, subList);
            childCount++;
        });
        newTask.appendChild(subList);
    }    
}

function getTextFromValue(elem_){
    //log("getTextFromValue -> " + elem_.getAttribute("data-name-task") + " | " + elem_.className);
    let itemList = eval(elem_.className.replace("-", "."));
    return itemList[elem_.getAttribute("property")];
}

function go() {
    taskList = getTaskList();
    if (typeof(taskList) === "undefined" || taskList == null) {
        initTaskList();
    }
    loadProject();
}


function listToJSON() {
    log("Serialisiere taskList...");
    let root = document.getElementById("tasklist");
    let obj = {};
    
    /* Projekt-Daten */
    let node = root.querySelector("div.projekt");
    obj["projekt"] = node.getAttribute("data-projekt-name");
    obj["datum"] = node.getAttribute("data-projekt-datum");

    /* Tasks */
    node = root.querySelector(listType);
    obj["tasks"] = ulToObject(node);
    return obj;
}

function ulToObject(elem_) {
    let obj = [];
    let elements = elem_.children;
    if ( elements.length > 0 ) {
        for( let i = 0; i < elements.length; ++i ) {
            let task = {};
            let el = elements[i];
            /*
             * Wenn das Child == LI --> Task raus parsen 
             * Wenn das Child == UL --> ulToObject()
             */
            if (el.tagName == "LI" ) {
                let el_child = el.firstElementChild;
                let name = el_child.getAttribute("data-task-name");
                let prio = el_child.getAttribute("data-task-prio");
                let status = el_child.getAttribute("data-task-status");
                if( name && prio && status ) {
                    task["name"] = name;
                    task["prio"] = parseInt(prio);
                    task["status"] = parseInt(status);
                }
                
                /* by default leere Liste */
                let subtasks = [];                
                if (el.lastChild.tagName == listType ) {
                    subtasks = ulToObject(el.lastChild);
                }

                task["tasks"] = subtasks;
                obj.push(task);
            }
        }
    }
    return obj;
}


/**
 * Baut Default-Aktivitäten für jedes LI / UL:
 * - 1 Ebene hoch   (arrow_left)
 * - 1 Ebene runter (arrow_right)
 * - Task + SubTasks 1 hoch (arrow_up)
 * - Task + SubTasks 1 runter (arrow_down)
 * - Task löschen
 */
function buildTaskOptions(elem_) {
    
}
